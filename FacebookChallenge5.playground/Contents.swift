import UIKit
import XCTest

let alphabet = (UnicodeScalar("a").value...(UnicodeScalar("z").value)).map { String(UnicodeScalar($0)!) }

func countDecoding(_ value: String) -> Int {
    
    if value.count == 1 || value.count == 0 {
        return 1
    }
    
    var count = 0
    let characters = Array(value)
    if let char = characters.first, let _ = Int(String(char)) {
        count += countDecoding(String(value.dropFirst(1)))
    }
    
    let doubleStr = String(characters[0...1])
    if let intDoubleValue = Int(doubleStr), intDoubleValue <= 26 {
        count += countDecoding(String(value.dropFirst(2)))
    }
    
    return count
}

class SolutionTest: XCTestCase {
    static var allTests = [
        ("testCountingDigits", testCountingDigits )
    ]
    
    func testCountingDigits() {
        var digits = "1234"
        var count = countDecoding(digits)
        XCTAssertEqual(count, 3)
        
        digits = "121"
        count = countDecoding(digits)
        XCTAssertEqual(count, 3)
        
        digits = "121212"
        count = countDecoding(digits)
        XCTAssertEqual(count, 13)
        
        digits = "111"
        count = countDecoding(digits)
        XCTAssertEqual(count, 3)
    }
}

class TestObserver: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase, didFailWithDescription description: String, inFile filePath: String?, atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let observer = TestObserver()
XCTestObservationCenter.shared.addTestObserver(observer)
SolutionTest.defaultTestSuite.run()
