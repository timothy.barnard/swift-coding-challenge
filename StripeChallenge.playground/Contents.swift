import UIKit
import XCTest

extension Array where Element == Int {
    mutating func bubbleSort() -> [Int] {
        for _ in 1..<self.count {
            for indexJ in 0..<self.count-1 {
                if self[indexJ] > self[indexJ + 1] {
                    let largerValue = self[indexJ+1]
                    let smallerValue = self[indexJ]
                    self[indexJ + 1] = smallerValue
                    self[indexJ] = largerValue
                }
            }
        }
        return self
    }
}


func findFirstMissingPositiveInteger(_ list: [Int]) -> Int? {
    var orderedList: [Int] = list.filter { $0 > 0}
    orderedList.bubbleSort()
    for index in 0..<orderedList.count-1 {
        let nextValue = orderedList[index] + 1
        if orderedList[index+1] != nextValue {
            return nextValue
            break
        }
    }
    return orderedList[orderedList.count-1] + 1
}

class SolutionTest: XCTestCase {
    static var allTests = [
        ("Bubble sort test", testBubbleSort),
        ("Stripe test", testExample),
    ]
    
    func testExample() {
        XCTAssertEqual(findFirstMissingPositiveInteger([3, 4, -1, 1]), 2, "Should return 2")
        XCTAssertEqual(findFirstMissingPositiveInteger([1, 2, 0]), 3, "Should return 3")
    }
    
    func testBubbleSort() {
        var firstList: [Int] = [3, 4, -1, 1]
        XCTAssertEqual(firstList.bubbleSort(), [-1, 1, 3, 4], "Should return ordered list")
        firstList = [1, 2, 0]
        XCTAssertEqual(firstList.bubbleSort(), [0, 1, 2], "Should return ordered list")
    }
}

class TestObserver: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase,
                  didFailWithDescription description: String,
                  inFile filePath: String?,
                  atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let testObserver = TestObserver()
XCTestObservationCenter.shared.addTestObserver(testObserver)
SolutionTest.defaultTestSuite.run()
