# Swift Coding Challenge

List of coding challenges

# 1.  array-challenge1.swift

>Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
>
>For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
>
>Bonus: Can you do this in one pass?


# 2. StatisticsTestPlayground.playground

>You are the "computer expert" of a local Athletic Association (C.A.A.). Many teams of runners come to compete. Each time you get a string of all race results of every team who >has run. For example here is a string showing the individual results of a team of 5 runners:
>
>"01|15|59, 1|47|6, 01|17|20, 1|32|34, 2|3|17"
>
>Each part of the string is of the form: h|m|s where h, m, s (h for hour, m for minutes, s for seconds) are positive or null integer (represented as strings) with one or two >digits. There are no traps in this format.
>
>To compare the results of the teams you are asked for giving three statistics; range, average and median.
>
>Range : difference between the lowest and highest values. In {4, 6, 9, 3, 7} the lowest value is 3, and the highest is 9, so the range is 9 − 3 = 6.
>
>Mean or Average : To calculate mean, add together all of the numbers in a set and then divide the sum by the total count of numbers.
>
>Median : In statistics, the median is the number separating the higher half of a data sample from the lower half. The median of a finite list of numbers can be found by >arranging all the observations from lowest value to highest value and picking the middle one (e.g., the median of {3, 3, 5, 9, 11} is 5) when there is an odd number of >observations. If there is an even number of observations, then there is no single middle value; the median is then defined to be the mean of the two middle values (the median >of {3, 5, 6, 9} is (5 + 6) / 2 = 5.5).
>
>Your task is to return a string giving these 3 values. For the example given above, the string result will be
>
>"Range: 00|47|18 Average: 01|35|15 Median: 01|32|34"
>
>of the form:
>
>"Range: hh|mm|ss Average: hh|mm|ss Median: hh|mm|ss"
>
>where hh, mm, ss are integers (represented by strings) with each 2 digits.


# 3. UberChallengePlayground.playground

>This problem was asked by Uber.
>
>Given an array of integers, return a new array such that each element at index i of the new array is the product of all >the numbers in the original array except the one at i.
>
>For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. 
>If our input was [3, 2, 1], the expected output would be [2, 3, 6].


# 4. StripeChallenge.playground

>This problem was asked by Stripe.
>
>Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer 
>that does not exist in the array. The array can contain duplicates and negative numbers as well.
>
>For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
>
>You can modify the input array in-place.


# 5. FacebookChallenge5.playground

>Good morning! Here's your coding interview problem for today.
>
>This problem was asked by Facebook.
>
>Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
>
>For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.
>
>You can assume that the messages are decodable. For example, '001' is not allowed.


# 6. GoogleChallenge6.playground

>Good morning! Here's your coding interview problem for today.

>This problem was asked by Google.

>A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.

>Given the root to a binary tree, count the number of unival subtrees.


# 7. AirbnbChallenge7.playground

>This problem was asked by Airbnb.
>
>Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.
>
>For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.


# 8. AppleChallenge8.playground 

>This problem was asked by Apple.
>
>Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.