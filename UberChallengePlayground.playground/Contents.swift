import UIKit
import XCTest

extension Array where Element: Equatable {
    
    mutating func filterIndex(_ index: Int) -> [Int] {
        return self.enumerated().filter { $0.offset != index }.map { $0.element } as? [Int] ?? []
    }
    
    mutating func productOfNumbers() -> [Int] {
        if self.count == 0 {return [Int]()}
        return self.enumerated().map { self.filterIndex($0.offset).reduce(1, *) }
    }
}

class SolutionTest: XCTestCase {
    static var allTests = [
        ("stat", testExample),
    ]
    
    func testExample() {
        var list: [Int] = [1, 2, 3, 4, 5]
        var solution: [Int] = [120, 60, 40, 30, 24]
        XCTAssertEqual(list.productOfNumbers(), solution, "should return \(solution)")
        
        list = [3, 2, 1]
        solution = [2, 3, 6]
        XCTAssertEqual(list.productOfNumbers(), solution, "should return \(solution)")
        
        list = [1, 2, 1, 3]
        solution = [6, 3, 6, 2]
        XCTAssertEqual(list.productOfNumbers(), solution, "should return \(solution)")
        
    }
    
}

class TestObserver: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase,
                  didFailWithDescription description: String,
                  inFile filePath: String?,
                  atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let testObserver = TestObserver()
XCTestObservationCenter.shared.addTestObserver(testObserver)
SolutionTest.defaultTestSuite.run()

