import UIKit
import XCTest

class Node<T> {
    var data: T
    var children: [Node] = []
    weak var parent: Node?
    
    init(_ data: T) {
        self.data = data
    }
    
    func printNodeData() -> [String] {
        return ["\(self.data)"] + self.children.flatMap{$0.printNodeData()}.map{"    "+$0}
    }
    func printTree() {
        let text = printNodeData().joined(separator: "\n")
        print(text)
    }
    
    func addNode(child: Node) {
        children.append(child)
        child.parent = self
    }
    
    func search(element: T) -> Node? {
        if "\(element)" == "\(self.data)"{
            return self
        }
        for child in children {
            if let result = child.search(element: element){
                return result
            }
        }
        return nil
    }
}

func countNoUinvalSubTress(_ node: Node<Int>) -> Int {
    
    var count = node.children.filter({ $0.data == node.data }).count == node.children.count ? 1 : 0
    
    for child in node.children {
        count += countNoUinvalSubTress(child)
    }
    
    return count
}

class SolutionTest: XCTestCase {
    
    static var allTests = [
        ("testOne", testOne),
        ("testTwo", testTwo)
    ]
    
    func testOne() {
        let root = Node<Int>(0)
        
        let aNode = Node<Int>(1)
        let bNode = Node<Int>(0)
        
        let cNode = Node<Int>(1)
        let dNode = Node<Int>(0)
        
        let eNode = Node<Int>(1)
        let fNode = Node<Int>(1)
        
        root.addNode(child: aNode)
        root.addNode(child: bNode)
        bNode.addNode(child: cNode)
        bNode.addNode(child: dNode)
        cNode.addNode(child: eNode)
        cNode.addNode(child: fNode)
        
        let result = countNoUinvalSubTress(root)
        XCTAssertEqual(result, 5)
    }
    
    func testTwo() {
        let root = Node<Int>(5)
        let aNode = Node<Int>(1)
        let bNode = Node<Int>(5)
        root.addNode(child: aNode)
        root.addNode(child: bNode)
        
        let cNode = Node<Int>(5)
        let dNode = Node<Int>(5)
        aNode.addNode(child: cNode)
        aNode.addNode(child: dNode)
        
        let eNode = Node<Int>(5)
        bNode.addNode(child: eNode)
        
        let result = countNoUinvalSubTress(root)
        XCTAssertEqual(result, 4)
    }
}


class TestObserver: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase,
                  didFailWithDescription description: String,
                  inFile filePath: String?,
                  atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let testObserver = TestObserver()
XCTestObservationCenter.shared.addTestObserver(testObserver)
SolutionTest.defaultTestSuite.run()
