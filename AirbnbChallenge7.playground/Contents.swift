import UIKit
import XCTest

func getlargestSumOfNonAdjacent(numbers: [Int]) -> Int {
    
    var including = numbers.first ?? 0
    var excluding = 0
    var excludingNew = 0
    for number in numbers.dropFirst(1) {
        excludingNew = (including > excluding) ? including : excluding
        including = excluding + number
        excluding = excludingNew
    }
    return (including > excluding) ? including : excluding
}

class SolutionTest: XCTestCase {
    static var tests = [
        ("testCaseOne", testCaseOne)
    ]
    
    func testCaseOne() {
        var data = [5,  5, 10, 40, 50, 35]
        var result = 80
        XCTAssertEqual(getlargestSumOfNonAdjacent(numbers: data), result)
        
        data = [2, 4, 6, 2, 5]
        result = 13
        XCTAssertEqual(getlargestSumOfNonAdjacent(numbers: data), result)
        
        data = [5, 1, 1, 5]
        result = 10
        XCTAssertEqual(getlargestSumOfNonAdjacent(numbers: data), result)
    }
}


class TestCaseObservation: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase, didFailWithDescription description: String, inFile filePath: String?, atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let testObserver = TestCaseObservation()
XCTestObservationCenter.shared.addTestObserver(testObserver)
SolutionTest.defaultTestSuite.run()
