let k = 17
let list = [10, 15, 3, 7]

func returnTwoNumbersAddTo(list: [Int], total: Int) -> (Int, Int)? {
	for val in list {
		let result: Double = Double(total) - Double(val)
		if result.truncatingRemainder(dividingBy: 1) == 0 {
			if let index = list.firstIndex(of: Int(result)) {
				return (val, list[index])
			}	
		}
	}
	return nil
}

if let result = returnTwoNumbersAddTo(list: list, total: k) {
	print("Found: \(result.0) + \(result.1) = \(k) ")
} else {
	print("The list provided does not added up to: \(k)")
}
