import UIKit
import XCTest

extension Array where Element == Int {
    
    func average() -> Int {
        return reduce(0) { $0 + $1 }/self.count
    }
    
    func range() -> Int {
        let max = self.max() ?? 0
        let min = self.min() ?? 0
        return max - min
    }
    
    func median() -> Int {
        let orderedList = self.sorted()
        let middleIndex = orderedList.count / 2
        if self.count % 2 == 1 {
            return orderedList[middleIndex]
        } else {
            let leftMiddleNo = orderedList[middleIndex-1]
            let rightMiddleNo = orderedList[middleIndex+1]
            return (leftMiddleNo + rightMiddleNo) / 2
        }
    }
}

extension Int {

    func secondsToHrMinSec() -> String {
        let hours = self / 3600
        let mins = self % 3600 / 60
        let seconds = self % 3600 % 60
        return "\(hours.padding)|\(mins.padding)|\(seconds.padding)"
    }
    
    var padding: String {
        return (self > 9) ? "\(self)" : "0\(self)"
    }
}

func stat(_ strg: String) -> String {
    let timeParts = strg.split(separator: ",")
    let secondList = timeParts.map({ (subString) -> Int in
        let splitTimes = subString.split(separator: "|")
        var seconds = Int(String(splitTimes[2])) ?? 0
        let mins = Int(String(splitTimes[1])) ?? 0
        let hours = Int(String(splitTimes[0])) ?? 0
        seconds += (mins * 60)
        seconds += ((hours * 60) * 60)
        return seconds
    })
    let average = "Average: \(secondList.average().secondsToHrMinSec())"
    let range = "Range: \(secondList.range().secondsToHrMinSec())"
    let mean = "Median: \(secondList.median().secondsToHrMinSec())"
    return "\(range) \(average) \(mean)"
}

class SolutionTest: XCTestCase {
    static var allTests = [
        ("stat", testExample),
    ]
    
    func dotest(_ s: String, _ expected: String) {
        XCTAssertEqual(stat(s), expected, "should return \(expected)")
    }
    
    func testExample() {
        
        var l = "01|15|59,01|47|16,01|17|20,01|32|34,02|17|17"
        var sol = "Range: 01|01|18 Average: 01|38|05 Median: 01|32|34"
        dotest(l,sol);

        l = "02|15|59,02|47|16,02|17|20,02|32|34,02|17|17,02|22|00,02|31|41"
        sol = "Range: 00|31|17 Average: 02|26|18 Median: 02|22|00"
        dotest(l,sol);

        l = "02|15|59,02|47|16,02|17|20,02|32|34,02|32|34,02|17|17"
        sol = "Range: 00|31|17 Average: 02|27|10 Median: 02|24|57"
        dotest(l,sol);
        
        l = "01|15|59,01|47|16,01|17|20,01|32|34,02|17|17"
        sol = "Range: 01|01|18 Average: 01|38|05 Median: 01|32|34"
        dotest(l, sol)
        
    }
    
}

class TestObserver: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase,
                  didFailWithDescription description: String,
                  inFile filePath: String?,
                  atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}

let testObserver = TestObserver()
XCTestObservationCenter.shared.addTestObserver(testObserver)
SolutionTest.defaultTestSuite.run()
