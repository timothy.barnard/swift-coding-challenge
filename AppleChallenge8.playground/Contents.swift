import UIKit
import XCTest

typealias Job = () -> Void

struct JobScheduler {
    func addJob(_ milliseconds: Int, job: Job) {
        sleep(UInt32(milliseconds / 1000))
        job()
    }
}

func printHello() {
    print("Hello")
}


class SolutionTest: XCTestCase {
    static var tests = [
        ("testExampleOne", testExampleOne)
    ]
    
    func testExampleOne() {
        let startTime = Date()
        let jobScheduler = JobScheduler()
        
        jobScheduler.addJob(5000, job: printHello)
        let components = Calendar.current.dateComponents([.second], from: startTime, to: Date())
        XCTAssertEqual(components.second, 5)
    }
    
    func testExampleTwo() {
        let startTime = Date()
        let jobScheduler = JobScheduler()
        
        jobScheduler.addJob(10000, job: printHello)
        let components = Calendar.current.dateComponents([.second], from: startTime, to: Date())
        XCTAssertEqual(components.second, 10)
    }
}


class TestObservation: NSObject, XCTestObservation {
    func testCase(_ testCase: XCTestCase, didFailWithDescription description: String, inFile filePath: String?, atLine lineNumber: Int) {
        assertionFailure(description, line: UInt(lineNumber))
    }
}


let observation = TestObservation()
XCTestObservationCenter.shared.addTestObserver(observation)
SolutionTest.defaultTestSuite.run()
